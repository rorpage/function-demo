using System;
using System.Linq;
using Amazon.Lambda.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Egineering.Function
{
  public static class FactorsAzure
  {
    [FunctionName("FactorsAzure")]
    public static IActionResult Run(
        [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
        ILogger log)
    {
      int value = string.IsNullOrWhiteSpace(req.Query["value"]) ?
        DateTime.Now.Year :
        int.Parse(req.Query["value"]);

      var output = new { value = value, factors = GetFactors(value) };
      return (ActionResult)new OkObjectResult(output);
    }

    [LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]
    public static object FunctionHandler(ILambdaContext context)
    {
      int value = DateTime.Now.Year;
      var output = new { value = value, factors = GetFactors(value) };
      return output;
    }

    private static int[] GetFactors(int value)
    {
      return Enumerable.Range(1, value).Where(i => value % i == 0).ToArray();
    }
  }
}
